# Momenton-CodePuzzle

Hi,
This is a console application developed in C# .Net 4.7.2,
The output of this app is an excel file containing the Employee Data in tabular structure. The structure is based on the managerial hierarchy.

Requirement:
1) SQL Server
2) Visual Studio 2019
3) .Net 4.7.2

How to Run the app?
1 - Run the QuickApp sln file in Visual Studio 2019.
2 - Restore all Nuget Packages.
3 - Modify the connection string in the app.config file.
4 - Run the app - This will automatically create the database and a table containing the required data. Please feel free to modify the data in the CompanyInitializer.cs file if required.

Thanks for reading my text.

Regards,
Ali
