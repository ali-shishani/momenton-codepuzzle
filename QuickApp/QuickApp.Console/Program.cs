﻿using ClosedXML.Excel;
using QuickApp.DAL;
using QuickApp.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickApp.Console
{
    class Program
    {

        static CompanyContext db;

        static void Main(string[] args)
        {
            InitialiseApp();
        }

        static void InitialiseApp()
        {
            // I will create the database, the table and insert the records in SQL server
            db = new CompanyContext();
            CompanyInitializer.InitialiseDatabase(db);
            RunApplication();
        }

        public static int rowNumber = 1;

        static void RunApplication()
        {
            System.Console.WriteLine("Application started. This application will create an excel file containing the employees of the company.");
            System.Console.WriteLine("Note: The data will be according to the managerial structure in a tabular format.");
            System.Console.WriteLine("Please press enter to start processing");
            System.Console.ReadLine();

            System.Console.WriteLine("Processing started, please wait...");
            var filePath = CreateEmployeeTabularData();
            System.Console.WriteLine("Thanks for waiting, processing complete.");

            System.Console.WriteLine("File is located in the following directory:");
            System.Console.WriteLine(filePath);
            System.Console.WriteLine("Thanks for running the app, please press enter to exit...");
            System.Console.ReadLine();
        }

        static string CreateEmployeeTabularData()
        {
            var filePath = Environment.CurrentDirectory + "/EmployeesData.xlsx";
            using (var workbook = new XLWorkbook())
            {
                var worksheet = workbook.Worksheets.Add("Employees Sheet");
                var allEmployees = db.Employees.OrderBy(e => e.ManagerId).ThenBy(e => e.Id).ToList();

                // first, I will get all top managers and process them one by one
                var allTopManagers = allEmployees.Where(e => e.ManagerId == null).ToList();
                if (allTopManagers.Any())
                {
                    for (int i = 0; i < allTopManagers.Count; i++)
                    {
                        var topManagerEmployee = allTopManagers[i];
                        ProcessEmployee(worksheet, topManagerEmployee, allEmployees, 1);
                    }

                    workbook.SaveAs(filePath);
                }
                else
                {
                    // if there is no top manager(s) then the data is invalid
                    System.Console.WriteLine("Invalid data! Could not find one or more top manager employee.");
                    System.Console.WriteLine("Please press enter to exit...");
                    System.Console.ReadLine();
                }
                
                if(allEmployees.Any())
                {
                    // if the list of employees still contain records, then the data must be invalid
                    System.Console.WriteLine("Invalid data! Data may contain invalid manager ID or other scenarios that makes the data invalid.");
                    System.Console.WriteLine("Please press enter to exit...");
                    System.Console.ReadLine();
                }
            }

            return filePath;
        }

        static void ProcessEmployee(IXLWorksheet worksheet, Employee processedEmployee, List<Employee> allEmployees, int columnNumber)
        {
            // create the record of the processed employee
            worksheet.Cell(rowNumber, columnNumber).Value = processedEmployee.Name;
            rowNumber++;

            // I will remove the processed employee from the list, so I can clean my processed data from the already processed records.
            // this will help me to validate the processed data. If I find remaining records by the end of my processing, then my processed
            // data must be invalid.
            allEmployees.Remove(processedEmployee);

            var allReportingEmployees = allEmployees.Where(e => e.ManagerId == processedEmployee.Id).ToList();
            for (int i = 0; i < allReportingEmployees.Count; i++)
            {
                // looks like the processed employee is a manager. I will process the reporting employees in this section 
                // using a recursive loop
                var currentColumnNumber = columnNumber + 1;
                var currentEmployee = allReportingEmployees[i];
                ProcessEmployee(worksheet, currentEmployee, allEmployees, currentColumnNumber);
            }
        }
    }
}
