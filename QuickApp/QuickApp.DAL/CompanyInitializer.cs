﻿using QuickApp.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuickApp.DAL
{
    public class CompanyInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<CompanyContext>
    {
        protected override void Seed(CompanyContext context)
        {
            InitialiseDatabase(context);
        }

        public static void InitialiseDatabase(CompanyContext context)
        {
            var allEmployees = context.Employees;
            context.Employees.RemoveRange(allEmployees);
            context.SaveChanges();

            var employees = new List<Employee>
            {
                new Employee{Id=100,Name="Alan",ManagerId= 150},
                new Employee{Id=220,Name="Martin",ManagerId= 100},
                new Employee{Id=150,Name="Jamie",ManagerId= null},
                new Employee{Id=275,Name="Alex",ManagerId= 100},
                new Employee{Id=400,Name="Steve",ManagerId= 150},
                new Employee{Id=190,Name="David",ManagerId= 400}
            };

            employees.ForEach(s => context.Employees.Add(s));
            context.SaveChanges();
        }
    }
}
